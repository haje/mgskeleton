def removeWW(s):
    return s.replace("\\", "/")

def wrap(s):
    return chr(34) + s + chr(34)

def subprocess_open(command):
    popen = subprocess.Popen(command, stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True)
    (stdoutdata, stderrdata) = popen.communicate()
    return stdoutdata, stderrdata

import os
import subprocess

try:
    f = open("mgcbpath", "r")
    mgcbpath = f.readline()
except:
    f = open("mgcbpath", "w")
    mgcbpath = input("enter mgcb file path: ")
    f.write(mgcbpath)
f.close()

os.chdir('Content')

# set buildtarget
projDir = os.getcwd()

buildtarget = []
for root, subdirs, files in os.walk('.'):
    for f in files:
        buildtarget.append(removeWW(os.path.join(root,f)))

print(buildtarget)

# run
outputDir = " /outputDir:../Android/Assets/Content"
interDir = " /intermediateDir:../Android/obj/mgcb"
option = " /build"
platform = " /platform:android"
buildDir = " /build:"
buildCmd = ""
for target in buildtarget:
    buildCmd = buildCmd + buildDir + wrap(target) + " "
exefile = wrap(mgcbpath + "/MGCB.exe")

a = [exefile, outputDir, interDir, option, platform, buildCmd]
s = ""
for x in a:
    s = s+x
print(s)

subprocess_open(s)
