import os
import subprocess

def subprocess_open(command):
    popen = subprocess.Popen(command, stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True)
    (stdoutdata, stderrdata) = popen.communicate()
    return stdoutdata, stderrdata

#build
os.system("build_PC.py")
os.system("build_Android.py")

#copy
WIN_cmd = "xcopy .\\Content\\bin\\windows .\\Win32\\Content /Y /I"
subprocess_open(WIN_cmd)

Android_cmd = "xcopy .\\Content\\bin\\android .\\Android\\Content /Y /I"
subprocess_open(Android_cmd)

#set project file
import codecs

#WIN32
WIN_list = []
try:
    os.chdir("WIN32/Content")
except:
    os.mkdir("WIN32/Content")
    os.chdir("WIN32/Content")
for root, subdirs, files in os.walk("."):
    for f in files:
        WIN_list.append(os.path.join(root, f)[2:])
os.chdir("../..")
WIN_proj = codecs.open("Win32/Win32.csproj", "r", encoding='utf-8')

lines = ""

mark = False
written = False
for line in WIN_proj.readlines():
    if not written:
        if line.find("<None") >= 0:
            lines = lines + line
            mark = True
            for target in WIN_list:
                if line.find("Content\\" + target) >= 0:
                    WIN_list.remove(target)

        elif mark and line.find("</ItemGroup>") >= 0:
            mark = False
            for target in WIN_list:
                lines = lines + "<None Include=\"Content\\" + target + "\">\r\n"
                lines = lines + "<CopyToOutputDirectory>PreserveNewest</CopyToOutputDirectory>\r\n"
                lines = lines + "</None>\r\n"
            written = True
            lines = lines + line

        elif not mark and line.find("</Project>") >= 0:
            lines = lines + "<ItemGroup>\r\n"
            for target in WIN_list:
                lines = lines + "<None Include=\"Content\\" + target + "\">\r\n"
                lines = lines + "<CopyToOutputDirectory>PreserveNewest</CopyToOutputDirectory>\r\n"
                lines = lines + "</None>\r\n"
            lines = lines + "</ItemGroup>\r\n"
            written = True
            lines = lines + line

        else:
            lines = lines + line
    else:
        lines = lines + line

WIN_proj.close()

WIN_proj = codecs.open("Win32/Win32.csproj", "w", encoding='utf-8')
WIN_proj.write(lines)
WIN_proj.close()

#ANDROID
Android_list = []
try:
    os.chdir("Android/Assets/Content")
except:
    os.mkdir("Android/Assets/Content")
    os.chdir("Android/Assets/Content")
for root, subdirs, files in os.walk("."):
    for f in files:
        Android_list.append(os.path.join(root, f)[2:])
os.chdir("../../..")
Android_proj = codecs.open("Android/Android.csproj", "r", encoding='utf-8')

lines = ""

mark = False
written = False
for line in Android_proj.readlines():
    if not written:
        if line.find("<AndroidAsset") >= 0:
            lines = lines + line
            mark = True
            for target in Android_list:
                if line.find("Assets\\Content\\" + target) >= 0:
                    Android_list.remove(target)

        elif mark and line.find("</ItemGroup>") >= 0:
            mark = False
            for target in Android_list:
                lines = lines + "<AndroidAsset Include=\"Assets\\Content\\" + target + "\">\r\n"
                lines = lines + "<CopyToOutputDirectory>PreserveNewest</CopyToOutputDirectory>\r\n"
                lines = lines + "</AndroidAsset>\r\n"
            written = True
            lines = lines + line

        elif not mark and line.find("</Project>") >= 0:
            lines = lines + "<ItemGroup>\r\n"
            for target in Android_list:
                lines = lines + "<AndroidAsset Include=\"Assets\\Content\\" + target + "\">\r\n"
                lines = lines + "<CopyToOutputDirectory>PreserveNewest</CopyToOutputDirectory>\r\n"
                lines = lines + "</AndroidAsset>\r\n"
            lines = lines + "</ItemGroup>\r\n"
            written = True
            lines = lines + line

        else:
            lines = lines + line
    else:
        lines = lines + line

Android_proj.close()

Android_proj = codecs.open("Android/Android.csproj", "w", encoding='utf-8')
Android_proj.write(lines)
Android_proj.close()